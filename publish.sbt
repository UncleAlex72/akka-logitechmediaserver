ThisBuild / organization := "uk.co.unclealex"
ThisBuild / organizationName := "unclealex"
ThisBuild / organizationHomepage := Some(url("https://bitbucket.org/UncleAlex72/"))

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/UncleAlex72/akka-logitechmediacenter"),
    "scm:git@bitbucket.org:UncleAlex72/akka-logitechmediacenter.git"
  )
)
ThisBuild / developers := List(
  Developer(
    id    = "1",
    name  = "Alex Jones",
    email = "alex.jones@unclealex.co.uk",
    url   = url("https://bitbucket.org/UncleAlex72/")
  )
)

ThisBuild / description := "Useful base libraries for interfacing with a Logitech Media Center with Scala via Akka streams."
ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
ThisBuild / homepage := Some(url("https://github.com/example/project"))

// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
  else Some("releases" at nexus + "service/local/staging/deploy/maven2")
}
ThisBuild / publishMavenStyle := true
