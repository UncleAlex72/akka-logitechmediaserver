package uk.co.unclealex.squeezebox

import scala.concurrent.Future

/**
  * A client for the Logitech Media Server.
  */
trait MediaServerClient {

  /**
    * Execute a program on a media server.
    * @param program The program to execute.
    * @tparam A The return type expected.
    * @return A future containing the eventual result.
    */
  def execute[A](program: SqueezeboxProgram[A]): Future[A]
}
