package uk.co.unclealex

import cats.free.Free

package object squeezebox {

  /**
    * A convenience type for programs that can be sent to a Logitech Media Sever. A free monad was chosen so that it
    * allows programs to be written in a more imperative style.
    * @tparam A The return type of the program.
    */
  type SqueezeboxProgram[A] = Free[SqueezeboxProgramA, A]
}
