package uk.co.unclealex.squeezebox

import scala.concurrent.Future

/**
  * An exception that is thrown from inside [[AkkaStreamMediaServerClient]]. It is used to test what requests were
  * sent to a media server when a program fails.
  * @param eventualExpectedRequests The requests sent to the media server.
  * @param cause The cause of this exception.
  */
class SqueezeboxException(
                           val eventualExpectedRequests: Future[Seq[SqueezeboxRequest]],
                           val cause: Throwable) extends Exception(cause) {

}
