package uk.co.unclealex.squeezebox

import akka.Done
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueueWithComplete}
import akka.stream.{Materializer, OverflowStrategy}
import cats.instances.future._
import cats.~>
import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.squeezebox.MediaServer.MediaServer
import uk.co.unclealex.squeezebox.{SqueezeboxProgram => P, SqueezeboxProgramA => SPA, SqueezeboxRequest => Req}

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}

/**
  * The main workhorse class of this project. Take a [[SqueezeboxProgram]] and use it to communicate to a
  * Logitech Media Server.
  * @param serverFlowBuilder A builder that can be used to create a connection to a [[MediaServer]] for each invocation.
  * @param materializer The materializer used to materialize Akka flows.
  * @param ec The execution context used by futures.
  */
class AkkaStreamMediaServerClient(
                                   serverFlowBuilder: () => MediaServer)
                                 (implicit materializer: Materializer, ec: ExecutionContext)
  extends MediaServerClient with StrictLogging {

  /**
    * Generate both a compiler for [[SqueezeboxProgram]]s as well as an eventual list of all requests received by
    * the media server. The list of requests is meant for testing purposes.
    * @return A pair of a compiler and eventual sequence.
    */
  def impureCompilerAndEventualRequests(): (SPA ~> Future, Future[Seq[SqueezeboxRequest]]) = {

    /**
      * A mutable promise that will contain the most recent response received by the media server.
      */
    @volatile var responsePromise: Promise[SqueezeboxResponse] = Promise[SqueezeboxResponse]

    /**
      * A sink used to take responses from the media server and feed them into the promise above.
      */
    val sink: Sink[SqueezeboxResponse, Future[Done]] = Sink.foreach { response =>
      logger.debug(s"Sink has received response $response")
      responsePromise.success(response)
    }

    /**
      * A queue that can be offered [[SqueezeboxRequest]]s
      */
    val source: Source[SqueezeboxRequest, SourceQueueWithComplete[SqueezeboxRequest]] =
      Source.queue(1, OverflowStrategy.backpressure)

    /**
      * This is the actual flow that will be used to communicate to the media center. It is built from both
      * the sink and the source.
      */
    val clientFlow: Flow[SqueezeboxResponse, SqueezeboxRequest, SourceQueueWithComplete[SqueezeboxRequest]] =
      Flow.fromSinkAndSourceCoupledMat(sink, source)(Keep.right)

    val (eventualRequests, queue): (Future[Seq[SqueezeboxRequest]], SourceQueueWithComplete[SqueezeboxRequest]) =
      serverFlowBuilder().joinMat(clientFlow)(Keep.both).run()

    val impureCompiler: SPA ~> Future = new (SPA ~> Future) {

      def reset(): Unit = {
        logger.debug(s"Resetting promise")
        responsePromise = Promise[SqueezeboxResponse]
      }

      def awaitResponse(request: SqueezeboxRequest): Future[SqueezeboxResponse] = {
        logger.debug(s"Waiting for a response for request $request")
        responsePromise.future
      }

      /**
        * Execute a command by first sending a request and then waiting for a response.
        * @param command The command to execute.
        * @tparam T The expected return type.
        * @return Eventually, the return value.
        */
      def execute[T](command: SqueezeboxCommand[T]): Future[T] = {
        val request: SqueezeboxRequest = command.request
        for {
          _ <- {
            logger.debug(s"Offering request $request")
            queue.offer(request)
          }
          response <- awaitResponse(request)
        } yield {
          logger.debug(s"Resetting after receiving response $response")
          reset()
          command.result(response) match {
            case Success(value) => value
            case Failure(exception) => throw exception
          }
        }
      }

      def apply[A](spa: SPA[A]): Future[A] = {
        val step: Future[_] = spa match {
          case cmd: CommandBasedSqueezeboxProgramA[A] =>
            execute(cmd.command)
          case SPA.Pure(value) =>
            Future.successful(value)
        }
        step.map(_.asInstanceOf[A]).recoverWith {
          case ex =>
            queue.offer(Req.Exit).flatMap { _ =>
              Future.failed(new SqueezeboxException(eventualRequests, ex))
            }
        }
      }
    }
    (impureCompiler, eventualRequests)
  }

  override def execute[A](program: SqueezeboxProgram[A]): Future[A] = {
    executeAndExposeRequests(program).map(_._1)
  }

  /**
    * Execute a program and also eventually return the list of [[SqueezeboxRequest]]s sent to the media server.
    * @param program The program to execute.
    * @tparam A The type of value being returned.
    * @return Eventually, a pair of the value returned and all sent requests.
    */
  def executeAndExposeRequests[A](program: SqueezeboxProgram[A]): Future[(A, Seq[SqueezeboxRequest])] = {
    val programWithExit: SqueezeboxProgram[A] = for {
      result <- program
      _ <- P.exit()
    } yield result
    val (impureCompiler, eventualRequests) = impureCompilerAndEventualRequests()
    for {
      result <- programWithExit.foldMap(impureCompiler)
      requests <- eventualRequests
    } yield {
      (result, requests)
    }
  }
}
