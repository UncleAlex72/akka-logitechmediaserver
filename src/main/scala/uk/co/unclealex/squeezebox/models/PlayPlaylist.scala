package uk.co.unclealex.squeezebox.models

/**
  * The playlist this is about to be played on a player.
  *
  * @param playerId The player that is playing the playlist.
  * @param playlistUrl The URL of the playlist being played.
  * @param playlistName The name of the playlist being played.
  */
case class PlayPlaylist(playerId: String, playlistUrl: String, playlistName: String)
