package uk.co.unclealex.squeezebox.models

/**
  * A playlist on the squeezebox server.
  *
  * @param id The ID of the playlist.
  * @param url The URL of the playlist.
  * @param name The name of the playlist.
  */
case class Playlist(id: String, url: String, name: String)
