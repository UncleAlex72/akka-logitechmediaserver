package uk.co.unclealex.squeezebox.models

/**
  * The album that is about to be played on a player.
  * @param playerId The ID of the player on which the album is about to be played.
  * @param title The title of the album.
  * @param artist The album's artist.
  */
case class PlayAlbum(playerId: String, title: String, artist: String)
