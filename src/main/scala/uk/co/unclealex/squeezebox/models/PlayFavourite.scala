package uk.co.unclealex.squeezebox.models

/**
  * The favourite that is about to be played on a player.
  * @param playerId The ID of the player on which the favourite is about to be played.
  * @param favouriteId The ID of the favourite.
  */
case class PlayFavourite(playerId: String, favouriteId: String)
