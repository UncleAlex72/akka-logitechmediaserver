package uk.co.unclealex.squeezebox.models

/**
  * The current track that is playing.
  *
  * @param title The title of the track.
  * @param maybeArtist The track's artist, if any.
  * @param maybeRemoteTitle The track's remote title, if any.
  */
case class NowPlaying(title: String, maybeArtist: Option[String], maybeRemoteTitle: Option[String])
