package uk.co.unclealex.squeezebox.models

/**
  * An album on the media center.
  *
  * @param id The ID of the album.
  * @param title The album's title.
  * @param artist The album's artist
  */
case class Album(id: String, title: String, artist: String)