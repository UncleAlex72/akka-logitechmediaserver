package uk.co.unclealex.squeezebox.models

import scala.concurrent.duration.FiniteDuration

/**
  * The messages that are about to be shown on a player.
  *
  * @param id The playerId of the player displaying the message.
  * @param topLine The message being displayed on the top line.
  * @param bottomLine The message being displayed on the bottom line.
  * @param duration The number of seconds for which the message will be displayed.
  */
case class DisplayMessage(id: String, topLine: String, bottomLine: String, duration: FiniteDuration)
