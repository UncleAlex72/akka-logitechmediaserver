package uk.co.unclealex.squeezebox.models

/**
  * A favourite on the squeezebox server.
  *
  * @param id The ID of the favourite.
  * @param name The name of the favourite.
  */
case class Favourite(id: String, name: String)
