package uk.co.unclealex.squeezebox.models

/**
  * A known squeezebox player.
  *
  * @param playerId The ID of the player.
  * @param name The name of the player.
  */
case class Player(playerId: String, name: String, connected: Boolean)
