package uk.co.unclealex.squeezebox

import akka.Done
import uk.co.unclealex.squeezebox.{SqueezeboxRequest => Req, SqueezeboxResponse => Res}

import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success, Try}

/**
  * A Squeezebox Command encapsulates a request sent to a media server as well as how to parse the expected response.
  * @param request The string request to send to the media server.
  * @tparam R The type of the response expected.
  */
sealed private[squeezebox] abstract case class SqueezeboxCommand[R](request: Req) {

  /**
    * Attempt to parse a response. This partial function should not attempt to parse response types it does not understand.
    * @return
    */
  protected def parse: PartialFunction[Res, R]

  /**
    * Get the value inside the response.
    * @param response The response received from the media server.
    * @return A Try that contains either the parsed response or a failure if the response does not tally with the request.
    */
  def result(response: Res): Try[R] = {
    parse.lift(response) match {
      case Some(validResponse) =>
        Success(validResponse)
      case None =>
        Failure(new IllegalStateException(s"response $response is not a valid response to $request"))
    }
  }
}

/**
  * The enumeration of all possible commands that can be sent to a media server.
  */
private[squeezebox] object SqueezeboxCommand {
  
  /**
    * A request for the number of connected players.
    */
  def countPlayers(): SqueezeboxCommand[Int] = new SqueezeboxCommand[Int](Req.CountPlayers) {
    override protected def parse: PartialFunction[Res, Int] = {
      case Res.CountPlayers(count) => count
    }
  }

  /**
    * A request for the number of albums.
    */
  def countAlbums(): SqueezeboxCommand[Int] = new SqueezeboxCommand[Int](Req.CountAlbums) {
    override protected def parse: PartialFunction[Res, Int] = {
      case Res.CountAlbums(count) => count
    }
  }

  /**
    * A request for the number of artists.
    */
  def countArtists(): SqueezeboxCommand[Int] = new SqueezeboxCommand[Int](Req.CountArtists) {
    override protected def parse: PartialFunction[Res, Int] = {
      case Res.CountArtists(count) => count
    }
  }

  /**
    * A request to find out the current song being played.
    *
    * @param playerId The player to query.
    */
  def nowPlaying(playerId: String): SqueezeboxCommand[Option[models.NowPlaying]] =
    new SqueezeboxCommand[Option[models.NowPlaying]](Req.NowPlaying(playerId)) {
      override protected def parse: PartialFunction[Res, Option[models.NowPlaying]] = {
        case Res.NowPlaying(maybeNowPlaying) => maybeNowPlaying
      }
    }

  /**
    * Request a list of currently connected players.
    */
  def players(): SqueezeboxCommand[Seq[models.Player]] = new SqueezeboxCommand[Seq[models.Player]](Req.Players) {
    override protected def parse: PartialFunction[Res, Seq[models.Player]] = {
      case Res.Players(players) => players
    }
  }

  /**
    * Request a database rescan (a quick scan).
    */
  def rescan(): SqueezeboxCommand[Unit] = new SqueezeboxCommand[Unit](Req.Rescan) {
    override protected def parse: PartialFunction[Res, Unit] = {
      case Res.Rescan =>
    }
  }

  /**
    * Request a database rescan (a quick scan).
    */
  def rebuildCache(): SqueezeboxCommand[Unit] = new SqueezeboxCommand[Unit](Req.RebuildCache) {
    override protected def parse: PartialFunction[Res, Unit] = {
      case Res.RebuildCache =>
    }
  }

  /**
    * A request for the playerId of a player.
    *
    * @param index The index of the player who's playerId is required.
    */
  def playerId(index: Int): SqueezeboxCommand[String] =
    new SqueezeboxCommand[String](Req.PlayerId(index)) {
      override protected def parse: PartialFunction[Res, String] = {
        case Res.PlayerId(maybeId) => maybeId
      }
    }

  /**
    * Request a list of all albums.
    */
  def albums(): SqueezeboxCommand[Seq[models.Album]] =
    new SqueezeboxCommand[Seq[models.Album]](Req.Albums) {
      override protected def parse: PartialFunction[Res, Seq[models.Album]] = {
        case Res.Albums(albums) => albums
      }
    }

  /**
    * Request a list of all favourites.
    */
  def favourites(): SqueezeboxCommand[Seq[models.Favourite]] =
    new SqueezeboxCommand[Seq[models.Favourite]](Req.Favourites) {
      override protected def parse: PartialFunction[Res, Seq[models.Favourite]] = {
        case Res.Favourites(favourites) => favourites
      }
    }

  /**
    * Request a list of all playlists.
    */
  def playlists(): SqueezeboxCommand[Seq[models.Playlist]] =
    new SqueezeboxCommand[Seq[models.Playlist]](Req.Playlists) {
      override protected def parse: PartialFunction[Res, Seq[models.Playlist]] = {
        case Res.Playlists(playlists) => playlists
      }
    }

  /**
    * Play an album.
    *
    * @param playerId The ID of the player that the album will be played on.
    * @param album    The title of the album.
    * @param artist   The name of the album's artist.
    */
  def playAlbum(playerId: String, album: String, artist: String): SqueezeboxCommand[models.PlayAlbum] =
    new SqueezeboxCommand[models.PlayAlbum](Req.PlayAlbum(playerId, album, artist)) {
      override protected def parse: PartialFunction[Res, models.PlayAlbum] = {
        case Res.PlayAlbum(playAlbum) => playAlbum
      }
    }

  /**
    * Play a favourite on a player.
    *
    * @param playerId    The player that will play the favourite.
    * @param favouriteId The favourite to play.
    */
  def playFavourite(playerId: String, favouriteId: String): SqueezeboxCommand[models.PlayFavourite] =
    new SqueezeboxCommand[models.PlayFavourite](Req.PlayFavourite(playerId, favouriteId)) {
      override protected def parse: PartialFunction[Res, models.PlayFavourite] = {
        case Res.PlayFavourite(playFavourite) => playFavourite
      }
    }

  /**
    * Play a playlist on a player.
    *
    * @param playerId     The player that will play the album.
    * @param playlistUrl  The URL of the playlist to play.
    * @param playlistName The name of the playlist to play.
    */
  def playPlaylist(playerId: String, playlistUrl: String, playlistName: String): SqueezeboxCommand[models.PlayPlaylist] =
    new SqueezeboxCommand[models.PlayPlaylist](Req.PlayPlaylist(playerId, playlistUrl, playlistName)) {
      override protected def parse: PartialFunction[Res, models.PlayPlaylist] = {
        case Res.PlayPlaylist(playPlaylist) => playPlaylist
      }
    }


  /**
    * A request to display a message on a player.
    *
    * @param playerId   The playerId of the player.
    * @param topLine    The message to display on the top line.
    * @param bottomLine The message to display on the bottom line.
    */
  def displayMessage(playerId: String, topLine: String, bottomLine: String, duration: FiniteDuration): SqueezeboxCommand[models.DisplayMessage] =
    new SqueezeboxCommand[models.DisplayMessage](Req.DisplayMessage(playerId, topLine, bottomLine, duration)) {
      override protected def parse: PartialFunction[Res, models.DisplayMessage] = {
        case Res.DisplayMessage(displayMessage) => displayMessage
      }
    }

  /**
    * A request to exit from the media server.
    */
  def exit(): SqueezeboxCommand[Done] = new SqueezeboxCommand[Done](Req.Exit) {
    override protected def parse: PartialFunction[Res, Done] = {
      case Res.Exit => Done
    }
  }
}