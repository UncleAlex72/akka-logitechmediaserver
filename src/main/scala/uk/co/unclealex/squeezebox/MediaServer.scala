package uk.co.unclealex.squeezebox

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Framing, Sink, Tcp}
import akka.util.ByteString
import com.typesafe.scalalogging.StrictLogging
import akka.NotUsed._
import uk.co.unclealex.squeezebox.Flows._

import scala.concurrent.Future

/**
  * A Factory object that has methods to connect to Logitech Media Servers.
  */
object MediaServer extends StrictLogging {

  /**
    * A convenience type that encapsulates a connection to a Media Server using a flow of squeezebox requests to
    * squeezebox responses. It also eventually materializes to a sequence of all the received requests, mainly for
    * testing purposes.
    */
  type MediaServer = Flow[SqueezeboxRequest, SqueezeboxResponse, Future[Seq[SqueezeboxRequest]]]

  /**
    * Generate a connection to media server.
    * @param host The host of the media server.
    * @param port The port the media server is listening on.
    * @param actorSystem The actor system used for akka streams.
    * @return A lazily-evaluated connection to a media server.
    */
  def apply(host: String, port: Int)(implicit actorSystem: ActorSystem): () => MediaServer = {
    val serverFlowBuilder: () => Flow[ByteString, ByteString, NotUsed] =
      () => Tcp().outgoingConnection(host, port).mapMaterializedValue(_ => notUsed)
    apply(serverFlowBuilder)
  }

  /**
    * Generate a connection to media server.
    * @param serverFlowBuilder An akka flow of byte strings, usually from a TCP connection.
    * @param actorSystem The actor system used for akka streams.
    * @return A lazily-evaluated connection to a media server.
    */
  def apply(serverFlowBuilder: () => Flow[ByteString, ByteString, _])(implicit actorSystem: ActorSystem): () => MediaServer = { () =>
    val _serverFlow: Flow[ByteString, SqueezeboxResponse, _] = serverFlowBuilder().map { raw =>
      logger.debug(s"Received raw: ${raw.utf8String}")
      raw
    }.via(Framing.delimiter(delimiter = ByteString('\n'), maximumFrameLength = Int.MaxValue, allowTruncation = true)).
      map(bs => bs.filter(by => by >= 32 && by <= 127)).
      map(_.utf8String.trim).
      filterNot(_.isEmpty).
      map { rawResponse =>
        logger.debug(s"Received from media server: $rawResponse")
        SqueezeboxResponse(rawResponse)
      }
    Flow.fromFunction[SqueezeboxRequest, ByteString](request => ByteString(s"${request.request}\n")).
      storeInputs(Sink.seq).
      via(_serverFlow)
  }

  /**
    * Create a mock media server that needs to be programmed how to respond to request. The mock server will
    * always respond correctly to an exit command.
    * @param fn A partial function that returns a [[SqueezeboxResponse]] depending on the [[SqueezeboxRequest]]
    *           sent to it.
    * @return A lazily-evaluated connection to the mock server.
    */
  def mock(fn: PartialFunction[SqueezeboxRequest, SqueezeboxResponse]): () => MediaServer = { () =>
    import scala.collection.immutable

    def optionToIterable[I](maybe: Option[I]): immutable.Iterable[I] = {
      maybe.fold(immutable.Seq.empty[I])(i => immutable.Seq(i))
    }

    val exiting: PartialFunction[SqueezeboxRequest, SqueezeboxResponse] = {
      case SqueezeboxRequest.Exit => SqueezeboxResponse.Exit
    }

    val serverLogic: SqueezeboxRequest => Option[SqueezeboxResponse] =  exiting.orElse(fn).lift

    Flow.fromFunction[SqueezeboxRequest, Option[SqueezeboxResponse]] { request =>
      logger.info(s"MediaServer received ${request.request}")
      serverLogic(request)
    }.
      async.
      mapConcat[SqueezeboxResponse](optionToIterable).
      storeInputs(Sink.seq).
      takeWhile(_ != SqueezeboxResponse.Exit, inclusive = true)
  }

}
