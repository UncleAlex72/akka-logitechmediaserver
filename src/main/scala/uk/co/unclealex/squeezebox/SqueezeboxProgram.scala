package uk.co.unclealex.squeezebox

import akka.Done
import cats.free.Free
import cats.free.Free.liftF
import uk.co.unclealex.squeezebox.SqueezeboxProgramA._

import scala.concurrent.duration.FiniteDuration

/**
  * An object that contains all the possible Squeezebox Programs.
  */
object SqueezeboxProgram {

  /**
    * A request for the number of connected players.
    */
  def countPlayers(): SqueezeboxProgram[Int] = liftF[SqueezeboxProgramA, Int](CountPlayers)

  /**
    * A request for the number of albums.
    */
  def countAlbums(): SqueezeboxProgram[Int] = liftF[SqueezeboxProgramA, Int](CountAlbums)

  /**
    * A request for the number of artists.
    */
  def countArtists(): SqueezeboxProgram[Int] = liftF[SqueezeboxProgramA, Int](CountArtists)

  /**
    * A request to find out the current song being played.
    *
    * @param playerId The player to query.
    */
  def nowPlaying(playerId: String): SqueezeboxProgram[Option[models.NowPlaying]] =
    liftF[SqueezeboxProgramA, Option[models.NowPlaying]](NowPlaying(playerId))

  /**
    * Request a list of currently connected players.
    */
  def players(): SqueezeboxProgram[Seq[models.Player]] = liftF[SqueezeboxProgramA, Seq[models.Player]](Players)

  /**
    * Request a database rescan (a quick scan).
    */
  def rescan(): SqueezeboxProgram[Unit] = liftF[SqueezeboxProgramA, Unit](Rescan)

  /**
    * Request a cache rebuild (a full scan).
    */
  def rebuildCache(): SqueezeboxProgram[Unit] = liftF[SqueezeboxProgramA, Unit](RebuildCache)

  /**
    * A request for the playerId of a player.
    *
    * @param index The index of the player who's playerId is required.
    */
  def playerId(index: Int): SqueezeboxProgram[String] = liftF[SqueezeboxProgramA, String](PlayerId(index))

  /**
    * Request a list of all albums.
    */
  def albums(): SqueezeboxProgram[Seq[models.Album]] = liftF[SqueezeboxProgramA, Seq[models.Album]](Albums)

  /**
    * Request a list of all favourites.
    */
  def favourites(): SqueezeboxProgram[Seq[models.Favourite]] = liftF[SqueezeboxProgramA, Seq[models.Favourite]](Favourites)

  /**
    * Request a list of all playlists.
    */
  def playlists(): SqueezeboxProgram[Seq[models.Playlist]] = liftF[SqueezeboxProgramA, Seq[models.Playlist]](Playlists)

  /**
    * Play an album.
    *
    * @param playerId The ID of the player that the album will be played on.
    * @param album    The title of the album.
    * @param artist   The name of the album's artist.
    */
  def playAlbum(playerId: String, album: String, artist: String): SqueezeboxProgram[models.PlayAlbum] =
    liftF[SqueezeboxProgramA, models.PlayAlbum](PlayAlbum(playerId, album, artist))

  /**
    * Play a favourite on a player.
    *
    * @param playerId    The player that will play the favourite.
    * @param favouriteId The favourite to play.
    */
  def playFavourite(playerId: String, favouriteId: String): SqueezeboxProgram[models.PlayFavourite] =
    liftF[SqueezeboxProgramA, models.PlayFavourite](PlayFavourite(playerId, favouriteId))

  /**
    * Play a playlist on a player.
    *
    * @param playerId     The player that will play the album.
    * @param playlistUrl  The URL of the playlist to play.
    * @param playlistName The name of the playlist to play.
    */
  def playPlaylist(playerId: String, playlistUrl: String, playlistName: String): SqueezeboxProgram[models.PlayPlaylist] =
    liftF[SqueezeboxProgramA, models.PlayPlaylist](PlayPlaylist(playerId, playlistUrl, playlistName))

  /**
    * A request to display a message on a player.
    *
    * @param playerId   The playerId of the player.
    * @param topLine    The message to display on the top line.
    * @param bottomLine The message to display on the bottom line.
    */
  def displayMessage(
                      playerId: String,
                      topLine: String,
                      bottomLine: String,
                      duration: FiniteDuration): SqueezeboxProgram[models.DisplayMessage] =
    liftF[SqueezeboxProgramA, models.DisplayMessage](DisplayMessage(playerId, topLine, bottomLine, duration))

  /**
    * A request to exit from the media server.
    */
  def exit(): SqueezeboxProgram[Done] = liftF[SqueezeboxProgramA, Done](Exit)

  /**
    * Run a command on each connected player.
    * @param commandBuilder A builder that can be used to construct a program for a particular player.
    * @tparam A The type the response from the built commands.
    * @return A map of responses keyed by the ID of the player it was executed on.
    */
  def forEachPlayer[A](commandBuilder: models.Player => SqueezeboxProgram[A]): SqueezeboxProgram[Map[String, A]] = {
    for {
      players <- players()
      result <- forEachPlayer(players, commandBuilder)
    } yield {
      result
    }
  }

  /**
    * Run a command on an explicit list of players.
    * @param commandBuilder A builder that can be used to construct a program for a particular player.
    * @tparam A The type the response from the built commands.
    * @return A map of responses keyed by the ID of the player it was executed on.
    */
  def forEachPlayer[A](
                        players: Seq[models.Player],
                        commandBuilder: models.Player => SqueezeboxProgram[A]): SqueezeboxProgram[Map[String, A]] = {
    val empty: SqueezeboxProgram[Map[String, A]] = Free.pure(Map.empty[String, A])
    players.foldLeft(empty) { (free, player) =>
      for {
        map <- free
        response <- commandBuilder(player)
      } yield map + (player.playerId -> response)
    }
  }

  /**
    * Convert a program into an optional program.
    * @param program The program to return if true.
    * @param flag A computation that decides whether the optional should be defined or not.
    * @tparam A The type of the program.
    * @return The given program if the flag is true, or an empty program otherwise.
    */
  def filter[A](program: SqueezeboxProgram[A], flag: => Boolean): SqueezeboxProgram[Option[A]] = {
    if (flag) {
      program.map(Some(_))
    }
    else {
      Free.pure(None)
    }
  }

  /**
    * Filter an optional program.
    * @param program The program to return if true.
    * @param flag A computation that decides whether the optional should be defined or not.
    * @tparam A The type of the program.
    * @return The given program if the flag is true, or an empty program otherwise.
    */
  def flatFilter[A](program: SqueezeboxProgram[Option[A]], flag: => Boolean): SqueezeboxProgram[Option[A]] = {
    if (flag) {
      program
    }
    else {
      Free.pure(None)
    }
  }
}
