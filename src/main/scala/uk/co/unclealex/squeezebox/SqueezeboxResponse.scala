package uk.co.unclealex.squeezebox

import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.squeezebox.models._
import Encoder._
import scala.concurrent.duration._

import scala.util.matching.Regex

/**
  * The difference responses that can be returned from the media centre.
  */
sealed trait SqueezeboxResponse

object SqueezeboxResponse extends StrictLogging {

  /**
    * The number of players connected to the media center.
    *
    * @param count The number of players connected.
    */
  case class CountPlayers(count: Int) extends SqueezeboxResponse
  object CountPlayers extends Parseable("player count ([0-9]+)") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(count) => CountPlayers(count.toInt)
    }
  }
  /**
    * Return the number of albums known to the media center.
    *
    * @param count The known number of albums.
    */
  case class CountAlbums(count: Int) extends SqueezeboxResponse
  object CountAlbums extends Parseable("albums", RegexBuilder.Counting) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(count) => CountAlbums(count.toInt)
    }
  }

  /**
    * Return the number of artists known to the media center.
    *
    * @param count The known number of artists.
    */
  case class CountArtists(count: Int) extends SqueezeboxResponse
  object CountArtists extends Parseable("artists", RegexBuilder.Counting) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(count) => CountArtists(count.toInt)
    }
  }

  /**
    * The current track that is playing an a squeezebox.
    *
    * @param maybeNowPlaying The current track being played, if any.
    */
  case class NowPlaying(maybeNowPlaying: Option[models.NowPlaying]) extends SqueezeboxResponse
  object NowPlaying extends Parseable("""([0-9a-fA-F%]+) status - 1 tags%3ANa (.+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(_, payload) => NowPlaying(parsePayload(payload) { fields =>
        for {
          title <- fields.get("title")
        } yield {
          models.NowPlaying(title, fields.get("artist"), fields.get("remote_title"))
        }
      }.headOption)
    }
  }


  /**
    * The playerId of the player with the given index.
    *
    * @param index The index of the player.
    */
  case class PlayerId(index: String) extends SqueezeboxResponse
  object PlayerId extends Parseable("""player id ([0-9]+) ([0-9a-fA-F%]+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(_, id) => PlayerId(decode(id))
    }
  }

  /**
    * Acknowledge that a message is being displayed by a player.
    *
    * @param displayMessage The playerId of the player displaying the message.
    */
  case class DisplayMessage(displayMessage: models.DisplayMessage) extends SqueezeboxResponse
  object DisplayMessage extends Parseable("""([0-9a-fA-F%]+) display (\S+) (\S+) ([0-9]+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(id, topLine, bottomLine, duration) =>
        DisplayMessage(
          models.DisplayMessage(decode(id), decode(topLine), decode(bottomLine), duration.toInt.seconds))

    }
  }

  /**
    * A list of albums on the media center
    *
    * @param albums The albums
    */
  case class Albums(albums: Seq[models.Album]) extends SqueezeboxResponse
  object Albums extends Parseable("albums [0-9]+ [0-9]+ tags%3Ala", RegexBuilder.ListingCountLast) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(payload, _) => Albums(parsePayload(payload) { fields =>
        for {
          id <- fields.get("id")
          title <- fields.get("album")
          artist <- fields.get("artist")
        } yield {
          Album(id, title, artist)
        }
      })
    }
  }

  /**
    * A response containing all connected players.
    *
    * @param players The list of connected players.
    */
  case class Players(players: Seq[models.Player]) extends SqueezeboxResponse
  object Players extends Parseable("players 0", RegexBuilder.ListingCountFirst) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(_, payload) =>
        Players(parsePayload(payload) { fields =>
          for {
            id <- fields.get("playerid")
            name <- fields.get("name")
            connected <- fields.get("connected")
          } yield {
            Player(id, name, connected == "1")
          }
        })
    }
  }

  /**
    * A response containing all the favourites on the server.
    *
    * @param favourites A list of all audio favourites.
    */
  case class Favourites(favourites: Seq[models.Favourite]) extends SqueezeboxResponse
  object Favourites extends Parseable("favorites items 0 99999999 tags%3Aname title%3AFavorites", RegexBuilder.ListingCountLast) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(payload, _) =>
        Favourites(parsePayload(payload) { fields =>
          for {
            id <- fields.get("id")
            name <- fields.get("name")
            isAudio <- fields.get("isaudio") if isAudio == "1"
          } yield {
            Favourite(id, name)
          }
        })
    }
  }

  /**
    * A response containing all playlists on the server.
    *
    * @param playlists The playlists on the server.
    */
  case class Playlists(playlists: Seq[models.Playlist]) extends SqueezeboxResponse
  object Playlists extends Parseable("playlists 0 99999999 tags%3Au", RegexBuilder.ListingCountLast) {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(payload, _) =>
        Playlists(parsePayload(payload) { fields =>
          for {
            id <- fields.get("id")
            url <- fields.get("url")
            name <- fields.get("playlist")
          } yield {
            Playlist(id, url, name)
          }
        })
    }
  }

  /**
    * The response to playing an album on a player.
    *
    * @param playAlbum The album that is about to be played.
    */
  case class PlayAlbum(playAlbum: models.PlayAlbum) extends SqueezeboxResponse
  object PlayAlbum extends Parseable("""([0-9a-fA-F%]+) playlist loadalbum \* (\S+) (\S+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(id, artist, title) =>
        PlayAlbum(models.PlayAlbum(decode(id), decode(title), decode(artist)))
    }
  }

  /**
    * The response to playing a favourite on a player.
    *
    * @param playFavourite The favourite that is about to be played.
    */
  case class PlayFavourite(playFavourite: models.PlayFavourite) extends SqueezeboxResponse
  object PlayFavourite extends Parseable("""([0-9a-fA-F%]+) favorites playlist play item_id%3A(\S+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(id, favouriteId) =>
        PlayFavourite(models.PlayFavourite(decode(id), decode(favouriteId)))
    }
  }

  /**
    * The response to playing a playlist on a player.
    *
    * @param playPlaylist The playlist that is about to be played.
    */
  case class PlayPlaylist(playPlaylist: models.PlayPlaylist) extends SqueezeboxResponse
  object PlayPlaylist extends Parseable("""([0-9a-fA-F%]+) playlist play (\S+) (\S+)""") {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re(id, url, name) =>
        PlayPlaylist(models.PlayPlaylist(decode(id), decode(url), decode(name)))
    }
  }

  /**
    * Acknowledge that a rescan has been requested.
    */
  object Rescan extends Parseable("rescan") with SqueezeboxResponse {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re() => Rescan
    }

    override def toString: String = "response.Rescan()"
  }

  /**
    * Acknowledge that a cache rebuild has been requested.
    */
  object RebuildCache extends Parseable("wipecache") with SqueezeboxResponse {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re() => RebuildCache
    }

    override def toString: String = "response.Rescan()"
  }

  /**
    * Acknowledge that the media centre connection is to be closed.
    */
  object Exit extends Parseable("exit") with SqueezeboxResponse {
    override def parser: PartialFunction[String, SqueezeboxResponse] = {
      case re() => Exit
    }

    override def toString: String = "response.Exit()"
  }


  private val parsers: Stream[String => Option[SqueezeboxResponse]] = Seq(
    CountPlayers.parser,
    CountAlbums.parser,
    CountArtists.parser,
    NowPlaying.parser,
    PlayerId.parser,
    DisplayMessage.parser,
    Albums.parser,
    Players.parser,
    Favourites.parser,
    Playlists.parser,
    PlayAlbum.parser,
    PlayFavourite.parser,
    PlayPlaylist.parser,
    Rescan.parser,
    RebuildCache.parser,
    Exit.parser
  ).map(_.lift).toStream

  def apply(str: String): SqueezeboxResponse = {
      parsers.flatMap(parser => parser(str)).headOption.getOrElse(Unknown(str))
  }

  /**
    * An unknown response from the media centre.
    *
    * @param text The full response.
    */
  case class Unknown(text: String) extends SqueezeboxResponse


  private[squeezebox] abstract class Parseable(regex: String, regexBuilder: RegexBuilder = RegexBuilder.None) {
    val re: Regex = regexBuilder(regex)

    def parser: PartialFunction[String, SqueezeboxResponse]

    def parsePayload[I](payload: String)(parser: Map[String, String] => Option[I]): Seq[I] = {
      def splitByColon(str: String): Option[(String, String)] = {
        val maybeFirstColonPosition: Option[Int] = Some(str.indexOf(':')).filter(_ != -1)
        maybeFirstColonPosition.map { firstColonPosition =>
          val (key, valueWithColon) = str.splitAt(firstColonPosition)
          val value: String = valueWithColon.drop(1)
          (key, value)
        }
      }

      def toMaps(response: Seq[(String, String)]): Seq[Map[String, String]] = {
        case class State(maybeDelimitingKey: Option[String] = None,
                         subLists: Seq[Map[String, String]] = Seq.empty,
                         currentPartition: Map[String, String] = Map.empty)
        val finalState: State = response.foldLeft(State()) { (state, kv) =>
          val (key, value) = kv
          val (subLists, currentPartition) = if (state.maybeDelimitingKey.contains(key)) {
            (state.subLists :+ state.currentPartition, Map.empty[String, String])
          }
          else {
            (state.subLists, state.currentPartition)
          }
          State(state.maybeDelimitingKey.orElse(Some(key)), subLists, currentPartition + (key -> value))
        }
        finalState.subLists :+ finalState.currentPartition
      }

      val fieldSequence: Seq[(String, String)] = for {
        encodedSegment <- payload.trim().split("""\s+""")
        segment <- Some(decode(encodedSegment))
        pair <- splitByColon(segment)
      } yield pair
      val fieldMap: Seq[Map[String, String]] = toMaps(fieldSequence)
      fieldMap.flatMap(parser(_).toSeq)
    }
  }

  private[squeezebox] class RegexBuilder(suffix: String) {
    def apply(re: String): Regex = (re + suffix).r
  }

  object RegexBuilder {
    object None extends RegexBuilder("")
    object Counting extends RegexBuilder("""\s+count%3A([0-9]+)""")
    object ListingCountFirst extends RegexBuilder("""\s+count%3A([0-9]+)\s+(.+)""")
    object ListingCountLast extends RegexBuilder("""\s+(.+?)\s+count%3A([0-9]+)""")
  }
}