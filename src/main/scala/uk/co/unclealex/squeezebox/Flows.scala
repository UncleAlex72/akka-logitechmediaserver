package uk.co.unclealex.squeezebox

import akka.stream.{FlowShape, UniformFanOutShape}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink}

import scala.concurrent.Future

/**
  * Syntactic sugar for flows that lets inputs and outputs be stored in a sink without affecting the underlying flow.
  */
object Flows {

  implicit class FlowImplicits[IN, OUT, MAT](flow: Flow[IN, OUT, MAT]) {

    private def storingFlow[V, M](sink: Sink[V, M]): Flow[V, V, M] = {
      import GraphDSL.Implicits._
      Flow.fromGraph(GraphDSL.create(sink) { implicit builder =>sinkShape =>
        val broadcast: UniformFanOutShape[V, V] = builder.add(Broadcast[V](2))

        // store each element using the supplied sink.
        broadcast.out(1) ~> sinkShape.in

        FlowShape(broadcast.in, broadcast.out(0))
      })
    }

    def storeOutputs[M2](sink: Sink[OUT, M2]): Flow[IN, OUT, M2] = {
      flow.viaMat(storingFlow(sink))(Keep.right)
    }

    def storeInputs[M1](sink: Sink[IN, M1]): Flow[IN, OUT, M1] = {
      storingFlow(sink).via(flow)
    }
  }
}
