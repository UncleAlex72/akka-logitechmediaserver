package uk.co.unclealex.squeezebox

import akka.stream.Materializer
import cats.free.Free
import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.squeezebox.MediaServer.MediaServer
import uk.co.unclealex.squeezebox.{SqueezeboxProgram => P}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Allow the squeezebox to be pinged.
  * @param serverFlowBuilder The builder used to generate the server flow.
  */
class SqueezeboxPing(
                      serverFlowBuilder: () => MediaServer)
                    (implicit materializer: Materializer, ec: ExecutionContext) extends StrictLogging {

  val mediaServerClient: MediaServerClient = new AkkaStreamMediaServerClient(serverFlowBuilder)

  def ping(): Future[String] = {
    val program: Free[SqueezeboxProgramA, Int] = for {
      playerCount <- P.countPlayers()
    } yield {
      playerCount
    }
    mediaServerClient.execute(program).map(_ => "ok")
  }
}

object SqueezeboxPing {

  def apply(serverFlowBuilder: () => MediaServer)
           (implicit materializer: Materializer, ec: ExecutionContext): () => Future[String] = {
    val squeezeboxPing = new SqueezeboxPing(serverFlowBuilder)
    squeezeboxPing.ping
  }
}