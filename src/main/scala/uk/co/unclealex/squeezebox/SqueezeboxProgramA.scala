package uk.co.unclealex.squeezebox

import akka.Done
import uk.co.unclealex.squeezebox.models.{Album, Favourite, Player, Playlist}

import scala.concurrent.duration.FiniteDuration
import uk.co.unclealex.squeezebox.{SqueezeboxCommand => Cmd}

/**
  * A type that can be wrapped in a free monad to produce a [[SqueezeboxProgram]]
  * @tparam A The type of the response expected from the program.
  */
private[squeezebox] sealed trait SqueezeboxProgramA[A]

private[squeezebox] sealed trait CommandBasedSqueezeboxProgramA[A] extends SqueezeboxProgramA[A] {
  val command: Cmd[A]
}

private[squeezebox] object SqueezeboxProgramA {

  /**
    * A request for the number of connected players.
    */
  case object CountPlayers extends CommandBasedSqueezeboxProgramA[Int] {
    val command: Cmd[Int] = Cmd.countPlayers()
  }

  /**
    * A request for the number of albums.
    */
  case object CountAlbums extends CommandBasedSqueezeboxProgramA[Int] {
    val command: Cmd[Int] = Cmd.countAlbums()
  }

  /**
    * A request for the number of artists.
    */
  case object CountArtists extends CommandBasedSqueezeboxProgramA[Int] {
    val command: Cmd[Int] = Cmd.countArtists()
  }

  /**
    * A request to find out the current song being played.
    *
    * @param playerId The player to query.
    */
  case class NowPlaying(playerId: String) extends CommandBasedSqueezeboxProgramA[Option[models.NowPlaying]] {
    val command: Cmd[Option[models.NowPlaying]] = Cmd.nowPlaying(playerId)
  }

  /**
    * Request a list of currently connected players.
    */
  case object Players extends CommandBasedSqueezeboxProgramA[Seq[models.Player]] {
    val command: Cmd[Seq[Player]] = Cmd.players()
  }

  /**
    * Request a database rescan (a quick scan).
    */
  case object Rescan extends CommandBasedSqueezeboxProgramA[Unit] {
    val command: Cmd[Unit] = Cmd.rescan()
  }

  /**
    * Request a cache rebuild (a full scan).
    */
  case object RebuildCache extends CommandBasedSqueezeboxProgramA[Unit] {
    val command: Cmd[Unit] = Cmd.rebuildCache()
  }

  /**
    * A request for the playerId of a player.
    *
    * @param index The index of the player who's playerId is required.
    */
  case class PlayerId(index: Int) extends CommandBasedSqueezeboxProgramA[String] {
    val command: Cmd[String] = Cmd.playerId(index)
  }

  /**
    * Request a list of all albums.
    */
  case object Albums extends CommandBasedSqueezeboxProgramA[Seq[models.Album]] {
    val command: Cmd[Seq[Album]] = Cmd.albums()
  }

  /**
    * Request a list of all favourites.
    */
  case object Favourites extends CommandBasedSqueezeboxProgramA[Seq[models.Favourite]] {
    val command: Cmd[Seq[Favourite]] = Cmd.favourites()
  }

  /**
    * Request a list of all playlists.
    */
  case object Playlists extends CommandBasedSqueezeboxProgramA[Seq[models.Playlist]] {
    val command: Cmd[Seq[Playlist]] = Cmd.playlists()
  }

  /**
    * Play an album.
    *
    * @param playerId The ID of the player that the album will be played on.
    * @param album    The title of the album.
    * @param artist   The name of the album's artist.
    */
  case class PlayAlbum(
                        playerId: String,
                        album: String,
                        artist: String) extends CommandBasedSqueezeboxProgramA[models.PlayAlbum] {
    val command: Cmd[models.PlayAlbum] = Cmd.playAlbum(playerId, album, artist)
  }

  /**
    * Play a favourite on a player.
    *
    * @param playerId    The player that will play the favourite.
    * @param favouriteId The favourite to play.
    */
  case class PlayFavourite(
                            playerId: String,
                            favouriteId: String) extends CommandBasedSqueezeboxProgramA[models.PlayFavourite] {
    val command: Cmd[models.PlayFavourite] = Cmd.playFavourite(playerId, favouriteId)
  }

  /**
    * Play a playlist on a player.
    *
    * @param playerId     The player that will play the album.
    * @param playlistUrl  The URL of the playlist to play.
    * @param playlistName The name of the playlist to play.
    */
  case class PlayPlaylist(
                           playerId: String,
                           playlistUrl: String,
                           playlistName: String) extends CommandBasedSqueezeboxProgramA[models.PlayPlaylist] {
    val command: Cmd[models.PlayPlaylist] = Cmd.playPlaylist(playerId, playlistUrl, playlistName)
  }

  /**
    * A request to display a message on a player.
    *
    * @param playerId   The playerId of the player.
    * @param topLine    The message to display on the top line.
    * @param bottomLine The message to display on the bottom line.
    */
  case class DisplayMessage(
                             playerId: String,
                             topLine: String,
                             bottomLine: String,
                             duration: FiniteDuration) extends CommandBasedSqueezeboxProgramA[models.DisplayMessage] {
    val command: Cmd[models.DisplayMessage] = Cmd.displayMessage(playerId, topLine, bottomLine, duration)
  }

  /**
    * A request to exit from the media server.
    */
  case object Exit extends CommandBasedSqueezeboxProgramA[Done] {
    val command: Cmd[Done] = Cmd.exit()
  }

  /**
   * A dummy request that just returns the given value. Used to build more complex programs.
   */
  case class Pure[A](value: A) extends SqueezeboxProgramA[A]

}