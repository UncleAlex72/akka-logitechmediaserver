package uk.co.unclealex.squeezebox

import scala.concurrent.duration.FiniteDuration
import Encoder._

/**
  * The different requests that can be sent to a squeezebox.
  *
  * @param request The request string to send to the squeezebox.
  */
sealed abstract class SqueezeboxRequest(val request: String)

/**
  * All the different types of possible requests.
  */
object SqueezeboxRequest {

  /**
    * A request for the number of connected players.
    */
  case object CountPlayers extends SqueezeboxRequest("player count ?")

  /**
    * A request for the number of albums.
    */
  case object CountAlbums extends SqueezeboxRequest("albums")

  /**
    * A request for the number of artists.
    */
  case object CountArtists extends SqueezeboxRequest("artists")

  /**
    * A request to find out the current song being played.
    *
    * @param playerId The player to query.
    */
  case class NowPlaying(playerId: String) extends SqueezeboxRequest(s"${encode(playerId)} status - 1 tags:Na")

  /**
    * Request a list of currently connected players.
    */
  case object Players extends SqueezeboxRequest(s"players 0")

  /**
    * A request for the playerId of a player.
    *
    * @param index The index of the player who's playerId is required.
    */
  case class PlayerId(index: Int) extends SqueezeboxRequest(s"player playerId $index ?")

  /**
    * Request a list of all albums.
    */
  object Albums extends SqueezeboxRequest(s"albums 0 99999999 tags:la")

  /**
    * Request a list of all favourites.
    */
  object Favourites extends SqueezeboxRequest("favorites items 0 99999999 tags:name")

  /**
    * Request a list of all playlists.
    */
  object Playlists extends SqueezeboxRequest("playlists 0 99999999 tags:u")

  /**
    * Play an album.
    *
    * @param playerId The ID of the player that the album will be played on.
    * @param album    The title of the album.
    * @param artist   The name of the album's artist.
    */
  case class PlayAlbum(playerId: String, album: String, artist: String) extends SqueezeboxRequest(
    s"${encode(playerId)} playlist loadalbum * ${encode(artist)} ${encode(album)}")

  /**
    * Play a favourite on a player.
    *
    * @param playerId    The player that will play the favourite.
    * @param favouriteId The favourite to play.
    */
  case class PlayFavourite(playerId: String, favouriteId: String) extends SqueezeboxRequest(
    s"${encode(playerId)} favorites playlist play item_id:${encode(favouriteId)}")

  /**
    * Play a playlist on a player.
    *
    * @param playerId     The player that will play the album.
    * @param playlistUrl  The URL of the playlist to play.
    * @param playlistName The name of the playlist to play.
    */
  case class PlayPlaylist(playerId: String, playlistUrl: String, playlistName: String) extends SqueezeboxRequest(
    s"${encode(playerId)} playlist play ${encode(playlistUrl)} ${encode(playlistName)}")

  /**
    * A request to display a message on a player.
    *
    * @param playerId   The playerId of the player.
    * @param topLine    The message to display on the top line.
    * @param bottomLine The message to display on the bottom line.
    */
  case class DisplayMessage(playerId: String, topLine: String, bottomLine: String, duration: FiniteDuration) extends SqueezeboxRequest(
    s"${encode(playerId)} display ${encode(topLine)} ${encode(bottomLine)} ${duration.toSeconds}")

  object DisplayMessage {

    /**
      * Display the same message on the top line and the bottom line.
      *
      * @param playerId The ID of the player that will display the message.
      * @param message  The message to display.
      * @param duration The amount of time to show the message.
      * @return A request to display the same message on the top and bottom lines.
      */
    def apply(playerId: String, message: String, duration: FiniteDuration): DisplayMessage = DisplayMessage(playerId, message, message, duration)
  }

  /**
    * A request to exit from the media server.
    */
  object Exit extends SqueezeboxRequest("exit") {
    override def toString: String = "Exit()"
  }

  /**
    * A request for a database rescan (a quick scan).
    */
  object Rescan extends SqueezeboxRequest("rescan") {
    override def toString: String = "Rescan()"
  }

  /**
    * A request for a database cache rebuild (a full scan).
    */
  object RebuildCache extends SqueezeboxRequest("wipecache") {
    override def toString: String = "WipeCache()"
  }

}