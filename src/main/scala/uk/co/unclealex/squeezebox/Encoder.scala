package uk.co.unclealex.squeezebox

import java.net.{URLDecoder, URLEncoder}
import java.nio.charset.StandardCharsets

/**
  * Escape and decode squeezebox commands.
  */
object Encoder {

  /**
    * Encode parts of a squeezebox command.
    * @param str The string to encode.
    * @return A URL encoded version of the original string.
    */
  def encode(str: String): String =
    URLEncoder.encode(str, StandardCharsets.UTF_8.name()).replace("+", "%20")

  /**
    * Decode encoded parts of a squeezebox command.
    * @param str The string to decode.
    * @return A plain text version of the encoded string.
    */
  def decode(str: String): String =
    URLDecoder.decode(str.replace("%20", "+"), StandardCharsets.UTF_8.name())

}
