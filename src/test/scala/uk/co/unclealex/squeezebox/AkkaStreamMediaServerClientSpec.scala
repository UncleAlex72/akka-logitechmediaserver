package uk.co.unclealex.squeezebox

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import org.scalatest.{Assertion, AsyncWordSpec, Matchers}
import uk.co.unclealex.squeezebox.MediaServer.MediaServer
import uk.co.unclealex.squeezebox.{SqueezeboxProgram => P, SqueezeboxRequest => Req, SqueezeboxResponse => Res}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class AkkaStreamMediaServerClientSpec extends AsyncWordSpec with Matchers {

  "Requesting a database rescan" should {
    "send a rescan request" in {
      P.rescan().shouldReturn({}).whilstSending(Req.Rescan, Req.Exit)
    }
  }

  "Requesting a cache rebuild" should {
    "send a cache rebuild request" in {
      P.rebuildCache().shouldReturn({}).whilstSending(Req.RebuildCache, Req.Exit)
    }
  }

  "Counting players" should {
    "count the number of players" in {
      P.countPlayers().shouldReturn(2).whilstSending(Req.CountPlayers, Req.Exit)
    }
  }

  "Counting albums" should {
    "count the number of albums" in {
      P.countAlbums().shouldReturn(1).whilstSending(Req.CountAlbums, Req.Exit)
    }
  }

  "Counting artists" should {
    "count the number of artists" in {
      P.countArtists().shouldReturn(1).whilstSending(Req.CountArtists, Req.Exit)
    }
  }

  "Querying what is now playing" should {
    "return the current track" in {
      P.nowPlaying("0").
        shouldReturn(Some(models.NowPlaying("title", Some("artist"), Some("remote_title")))).
        whilstSending(Req.NowPlaying("0"), Req.Exit)
    }
  }

  "Getting all the players" should {
    "return all the players" in {
      P.players().
        shouldReturn(Seq(models.Player("0", "Bedroom", connected = true), models.Player("1", "Kitchen", connected = true))).
        whilstSending(Req.Players, Req.Exit)
    }
  }

  "Getting a player ID" should {
    "return the correct ID" in {
      P.playerId(0).shouldReturn("0").whilstSending(Req.PlayerId(0), Req.Exit)
    }
  }

  "Getting all the albums" should {
    "return all the albums" in {
      P.albums().shouldReturn(Seq(models.Album("0", "Queen II", "Queen"))).whilstSending(Req.Albums, Req.Exit)
    }
  }

  "Getting all the favourites" should {
    "return all the favourites" in {
      P.favourites().
        shouldReturn(Seq(models.Favourite("0", "All Gojira Albums"))).
        whilstSending(Req.Favourites, Req.Exit)
    }
  }

  "Getting all the playlists" should {
    "return all the playlists" in {
      P.playlists().
        shouldReturn(Seq(models.Playlist("0", "http://playlist", "playlist"))).
        whilstSending(Req.Playlists, Req.Exit)
    }
  }

  "Playing an album" should {
    "return an acknowledgement" in {
      P.playAlbum("0", "Queen II", "Queen").
        shouldReturn(models.PlayAlbum("0", "Queen II", "Queen")).
        whilstSending(Req.PlayAlbum("0", "Queen II", "Queen"), Req.Exit)
    }
  }

  "Playing a favourite" should {
    "return an acknowledgement" in {
      P.playFavourite("0", "1").
        shouldReturn(models.PlayFavourite("0", "1")).
        whilstSending(Req.PlayFavourite("0", "1"), Req.Exit)
    }
  }

  "Playing a playlist" should {
    "return an acknowledgement" in {
      P.playPlaylist("0", "http://playlist", "playlist").
        shouldReturn(models.PlayPlaylist("0", "http://playlist", "playlist")).
        whilstSending(Req.PlayPlaylist("0", "http://playlist", "playlist"), Req.Exit)
    }
  }

  "Displaying a message" should {
    "return an acknowledgement" in {
      P.displayMessage("0", "top", "bottom", 30.seconds).
        shouldReturn(models.DisplayMessage("0", "top", "bottom", 30.seconds)).
        whilstSending(Req.DisplayMessage("0", "top", "bottom", 30.seconds), Req.Exit)
    }
  }

  "Chaining two programs" should {
    "execute them both sequentially" in {
      val program = for {
        albumCount <- P.countAlbums()
        playerCount <- P.countPlayers()
      } yield {
        (albumCount, playerCount)
      }
      program.shouldReturn((1, 2)).
        whilstSending(Req.CountAlbums, Req.CountPlayers, Req.Exit)
    }
  }

  "Repeating an action for each player" should {
    "Acknowledge responses when the call to get all the players is included" in {
      P.forEachPlayer(
        Seq(models.Player("0", "Bedroom", connected = true), models.Player("1", "Kitchen", connected = true)),
        player => P.displayMessage(player.playerId, "top", "bottom", 30.seconds)).
        shouldReturn(
          Map(
            "0" -> models.DisplayMessage("0", "top", "bottom", 30.seconds),
            "1" -> models.DisplayMessage("1", "top", "bottom", 30.seconds))).
        whilstSending(
          Req.DisplayMessage("0", "top", "bottom", 30.seconds),
          Req.DisplayMessage("1", "top", "bottom", 30.seconds), Req.Exit)
    }
  }

  "Filtering a program" should {
    "Run the whole program when the flag is true" in {
      val program = for {
        _ <- P.countAlbums()
        maybePlayerCount <- P.filter(P.countPlayers(), flag = true)
        _ <- P.countArtists()
      } yield {
        maybePlayerCount
      }
      program.shouldReturn(Some(2)).whilstSending(Req.CountAlbums, Req.CountPlayers, Req.CountArtists, Req.Exit)
    }
    "Not run the program when the flag is false" in {
      val program = for {
        _ <- P.countAlbums()
        maybePlayerCount <- P.filter(P.countPlayers(), flag = false)
        _ <- P.countArtists()
      } yield {
        maybePlayerCount
      }
      program.shouldReturn(None).whilstSending(Req.CountAlbums, Req.CountArtists, Req.Exit)
    }
  }

  "FlatFiltering a program" should {
    "Run the whole program when the flag is true and return a Some value when the program is a Some" in {
      val program = for {
        _ <- P.countAlbums()
        maybeNowPlaying <- P.flatFilter(P.nowPlaying("0"), flag = true)
        _ <- P.countArtists()
      } yield {
        maybeNowPlaying
      }
      program.
        shouldReturn(Some(models.NowPlaying("title", Some("artist"), Some("remote_title")))).
        whilstSending(Req.CountAlbums, Req.NowPlaying("0"), Req.CountArtists, Req.Exit)
    }
    "Not run the program when the flag is false and the result would have been a Some" in {
      val program = for {
        _ <- P.countAlbums()
        maybeNowPlaying <- P.flatFilter(P.nowPlaying("0"), flag = false)
        _ <- P.countArtists()
      } yield {
        maybeNowPlaying
      }
      program.shouldReturn(None).whilstSending(Req.CountAlbums, Req.CountArtists, Req.Exit)
    }
    "Run the whole program when the flag is true and return a None value when the program is a None" in {
      val program = for {
        _ <- P.countAlbums()
        maybeNowPlaying <- P.flatFilter(P.nowPlaying("1"), flag = true)
        _ <- P.countArtists()
      } yield {
        maybeNowPlaying
      }
      program.shouldReturn(None).whilstSending(Req.CountAlbums, Req.NowPlaying("1"), Req.CountArtists, Req.Exit)
    }
    "Not run the program when the flag is false and the result would have been a None" in {
      val program = for {
        _ <- P.countAlbums()
        maybeNowPlaying <- P.flatFilter(P.nowPlaying("1"), flag = false)
        _ <- P.countArtists()
      } yield {
        maybeNowPlaying
      }
      program.shouldReturn(None).whilstSending(Req.CountAlbums, Req.CountArtists, Req.Exit)
    }
  }

  "Repeating an action for each player" should {
    "Acknowledge responses when the call to get all the players is made beforehand" in {
      P.forEachPlayer(
        player => P.displayMessage(player.playerId, "top", "bottom", 30.seconds)).
        shouldReturn(
          Map(
            "0" -> models.DisplayMessage("0", "top", "bottom", 30.seconds),
            "1" -> models.DisplayMessage("1", "top", "bottom", 30.seconds))).
        whilstSending(
          Req.Players,
          Req.DisplayMessage("0", "top", "bottom", 30.seconds),
          Req.DisplayMessage("1", "top", "bottom", 30.seconds),
          Req.Exit)
    }
  }

  "An unexpected response" should {
    "fail quickly with an exception and still try to exit" in {
      implicit val actorSystem: ActorSystem = ActorSystem()
      implicit val materializer: Materializer = ActorMaterializer()
      val squeezebox = new AkkaStreamMediaServerClient(brokenServerBuilder())
      val program: SqueezeboxProgram[(Int, Int)] = for {
        albumCount <- P.countAlbums()
        playerCount <- P.countPlayers()
      } yield {
        (albumCount, playerCount)
      }

      squeezebox.executeAndExposeRequests(program).transformWith {
        case Success(_) =>
          Future.successful(fail("An error response should not be successful."))
        case Failure(ex) =>
          ex match {
            case sqe: SqueezeboxException =>
              sqe.eventualExpectedRequests.map { requests =>
                requests should contain theSameElementsInOrderAs Seq(Req.CountAlbums, Req.Exit)
              }
            case _ => Future.successful(fail(s"An exception of type ${ex.getClass} was thrown"))
          }
      }
    }
  }
  
  def mockServerBuilder(): () => MediaServer = {
    val players = Seq(models.Player("0", "Bedroom", connected = true), models.Player("1", "Kitchen", connected = true))
    val albums = Seq(models.Album("0", "Queen II", "Queen"))
    val playlists = Seq(models.Playlist("0", "http://playlist", "playlist"))
    val favourites = Seq(models.Favourite("0", "All Gojira Albums"))
    MediaServer.mock {
      case Req.Albums => Res.Albums(albums)
      case Req.CountAlbums => Res.CountAlbums(albums.length)
      case Req.CountArtists => Res.CountArtists(1)
      case Req.CountPlayers => Res.CountPlayers(players.length)
      case Req.Rescan => Res.Rescan
      case Req.RebuildCache => Res.RebuildCache
      case Req.Exit => Res.Exit
      case Req.Favourites => Res.Favourites(favourites)
      case Req.Players => Res.Players(players)
      case Req.Playlists => Res.Playlists(playlists)
      case Req.PlayerId(index) => Res.PlayerId(players(index).playerId)
      case Req.PlayAlbum(playerId, album, artist) =>
        Res.PlayAlbum(models.PlayAlbum(playerId, album, artist))
      case Req.PlayPlaylist(playerId, playlistName, playlistUrl) =>
        Res.PlayPlaylist(models.PlayPlaylist(playerId, playlistName, playlistUrl))
      case Req.PlayFavourite(playerId, favouriteId) =>
        Res.PlayFavourite(models.PlayFavourite(playerId, favouriteId))
      case Req.NowPlaying(playerId) =>
        Res.NowPlaying(Some(models.NowPlaying("title", Some("artist"), Some("remote_title"))).
          filter(_ => playerId == "0"))
      case Req.DisplayMessage(playerId, topLine, bottomLine, duration) =>
        Res.DisplayMessage(models.DisplayMessage(playerId, topLine, bottomLine, duration))
    }
  }

  def brokenServerBuilder(): () => MediaServer = MediaServer.mock {
    case _ => Res.Unknown("unknown")
  }

  class TestCase[A](program: SqueezeboxProgram[A], expectedResult: A) {

    def whilstSending(expectedRequests: Req*): Future[Assertion] = {
      implicit val actorSystem: ActorSystem = ActorSystem()
      implicit val materializer: Materializer = ActorMaterializer()
      val squeezebox = new AkkaStreamMediaServerClient(mockServerBuilder())
      squeezebox.executeAndExposeRequests(program).map {
        case (actualResult, actualRequests) =>
          expectedResult should ===(actualResult)
          expectedRequests should contain theSameElementsInOrderAs actualRequests
      }

    }
  }

  implicit class TestDSL[A](program: SqueezeboxProgram[A]) {

    def shouldReturn(expectedResult: A) = new TestCase(program, expectedResult)
  }
}
