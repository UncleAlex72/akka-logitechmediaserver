package uk.co.unclealex.squeezebox

import org.scalatest.{Matchers, WordSpec}
import uk.co.unclealex.squeezebox.SqueezeboxResponse._
import uk.co.unclealex.squeezebox.MediaServerClientResponseSpec._
import scala.concurrent.duration._

class MediaServerClientResponseSpec extends WordSpec with Matchers {

  "Parsing a player count response" should {
    "identify the number of players" in {
      parse("player count 5") should ===(CountPlayers(5))
    }
  }

  "Getting the ID of a player" should {
    "correctly decode the ID" in {
      parse("player id 0 09%3A14%3A2e%3Aef%3Ab3%3Aac") should ===(PlayerId("09:14:2e:ef:b3:ac"))
    }
  }

  "Parsing what is being displayed on a player" should {
    "decode the ID and both messages" in {
      parse("09%3A14%3A2e%3Aef%3Ab3%3Aac display Freddie+Mercury Brian+May 30") should ===(
        DisplayMessage(models.DisplayMessage("09:14:2e:ef:b3:ac", "Freddie Mercury", "Brian May", 30.seconds)))
    }
  }

  "Counting the number of albums" should {
    "correctly report how many albums are on the server" in {
      parse("albums   count%3A5") should ===(CountAlbums(5))
    }
  }

  "Counting the number of artists" should {
    "correctly report how many artists are on the server" in {
      parse("artists  count%3A5") should ===(CountArtists(5))
    }
  }

  "Finding what is currently playing" should {
    "Correctly identify what is playing from a stream" in {
      parse(planetRock) should ===(
        NowPlaying(Some(models.NowPlaying(
          "All Along The Watchtower",
          Some("Jimi Hendrix Experience"),
          Some("Planet Rock")))))
    }
    "Correctly identify what is playing from an album" in {
      parse(petSounds) should ===(
        NowPlaying(Some(models.NowPlaying("Wouldn't It Be Nice", Some("The Beach Boys"), None))))
    }
    "Correctly identify when nothing is playing" in {
      parse(nothing) should ===(NowPlaying(None))
    }
  }

  "Getting the list of players" should {
    "return the bedroom and kitchen" in {
      parse(players) should ===(
        Players(Seq(
          models.Player("00:01:02:03:04:05", "Kitchen", connected = true),
          models.Player("40:41:42:43:44:45", "Lounge", connected = false),
          models.Player("80:81:82:83:84:85", "Bedroom", connected = true)))
      )
    }
  }

  "Getting the list of albums" should {
    "return all the albums" in {
      parse(albums) should ===(
        Albums(Seq(
          models.Album("4482", "A Kind of Magic", "Queen"),
          models.Album("4481", "A Kind of Magic (Extras)", "Queen"),
          models.Album("4074", "Greatest Hits", "Queen"),
          models.Album("4484", "A Night at the Opera", "Queen"),
          models.Album("4483", "Greatest Hits", "The Police")))
      )
    }
  }

  "Getting the list of playlists" should {
    "return all the playlists" in {
      parse(playlists) should ===(
        Playlists(Seq(
          models.Playlist("99057", "file:///mnt/playlists/Mezmerize%20and%20Hypnotize.m3u", "Mezmerize and Hypnotize")))
      )
    }
  }

  "Getting the list of favourites" should {
    "return all the favourites" in {
      parse(favourites) should ===(
        Favourites(Seq(
          models.Favourite("0", "Planet Rock"),
          models.Favourite("1", "BBC Radio 5 live"),
          models.Favourite("2", "Infant")))
      )
    }
  }

  "Reporting that an album is being played" should {
    "correctly report the album being played" in {
      parse("09%3A14%3A2e%3Aef%3Ab3%3Aac playlist loadalbum * Queen Greatest%20Hits") should ===(
        PlayAlbum(models.PlayAlbum("09:14:2e:ef:b3:ac", "Greatest Hits", "Queen"))
      )
    }
  }

  "Reporting that a playlist is being played" should {
    "correctly report the playlist being played" in {
      parse("09%3A14%3A2e%3Aef%3Ab3%3Aac playlist play %2Fmnt%2Fplaylists%2FMezmerize%20and%20Hypnotize.m3u Mezmerize%20and%20Hypnotize") should ===(
        PlayPlaylist(models.PlayPlaylist("09:14:2e:ef:b3:ac", "/mnt/playlists/Mezmerize and Hypnotize.m3u", "Mezmerize and Hypnotize"))
      )
    }
  }

  "Reporting that a favourite is being played" should {
    "correctly report the favourite being played" in {
      parse("09%3A14%3A2e%3Aef%3Ab3%3Aac favorites playlist play item_id%3A5cf87fdc.0") should ===(
        PlayFavourite(models.PlayFavourite("09:14:2e:ef:b3:ac", "5cf87fdc.0"))
      )
    }
  }
}

object MediaServerClientResponseSpec {

  val parse: String => SqueezeboxResponse = SqueezeboxResponse.apply

  val players: String = """players 0  count%3A3
                                |playerindex%3A0
                                |playerid%3A00%3A01%3A02%3A03%3A04%3A05
                                |uuid%3A
                                |ip%3A192.168.1.119%3A32372
                                |name%3AKitchen
                                |seq_no%3A0
                                |model%3Asqueezebox2
                                |modelname%3ASqueezebox2
                                |power%3A1
                                |isplaying%3A1
                                |displaytype%3Agraphic-320x32
                                |isplayer%3A1
                                |canpoweroff%3A1
                                |connected%3A1
                                |firmware%3A137
                                |playerindex%3A1
                                |playerid%3A40%3A41%3A42%3A43%3A44%3A45
                                |uuid%3A
                                |ip%3A192.168.1.148%3A17827
                                |name%3ALounge
                                |seq_no%3A0
                                |model%3Asqueezebox
                                |modelname%3ASqueezebox
                                |power%3A1
                                |isplaying%3A0
                                |displaytype%3Agraphic-280x16
                                |isplayer%3A1
                                |canpoweroff%3A1
                                |connected%3A0
                                |firmware%3A40
                                |playerindex%3A2
                                |playerid%3A80%3A81%3A82%3A83%3A84%3A85
                                |uuid%3A
                                |ip%3A192.168.1.81%3A41304
                                |name%3ABedroom
                                |seq_no%3A0
                                |model%3Asqueezebox3
                                |modelname%3ASqueezebox%20Classic
                                |power%3A1
                                |isplaying%3A0
                                |displaytype%3Agraphic-320x32
                                |isplayer%3A1
                                |canpoweroff%3A1
                                |connected%3A1
                                |firmware%3A137""".stripMargin.replace('\n', ' ')

  val albums: String =
    """albums 0 5 tags%3Ala
      |id%3A4482 album%3AA%20Kind%20of%20Magic artist%3AQueen
      |id%3A4481 album%3AA%20Kind%20of%20Magic%20(Extras) artist%3AQueen
      |id%3A4074 album%3AGreatest%20Hits artist%3AQueen
      |id%3A4484 album%3AA%20Night%20at%20the%20Opera artist%3AQueen
      |id%3A4483 album%3AGreatest%20Hits artist%3AThe%20Police count%3A10""".stripMargin.replace('\n', ' ')

  val favourites: String =
    """favorites items 0 99999999 tags%3Aname title%3AFavorites
      |id%3A0 name%3APlanet%20Rock type%3Aaudio isaudio%3A1 hasitems%3A0
      |id%3A1 name%3ABBC%20Radio%205%20live type%3Aaudio isaudio%3A1 hasitems%3A0
      |id%3A2 name%3AInfant type%3Aplaylist isaudio%3A1 hasitems%3A1 count%3A3""".stripMargin.replace('\n', ' ')

  val playlists: String =
    "playlists 0 99999999 tags%3Au id%3A99057 playlist%3AMezmerize%20and%20Hypnotize url%3Afile%3A%2F%2F%2Fmnt%2Fplaylists%2FMezmerize%2520and%2520Hypnotize.m3u count%3A1"

  val planetRock: String =
    """09%3A14%3A2e%3Aef%3Ab3%3Aac status - 1 tags%3ANa
      |player_name%3AKitchen player_connected%3A1 player_ip%3A192.168.1.81%3A41101
      |power%3A1 signalstrength%3A0 mode%3Aplay remote%3A1 current_title%3APlanet%20Rock
      |time%3A326.048606931686 rate%3A1 mixer%20volume%3A100 playlist%20repeat%3A0
      |playlist%20shuffle%3A0 playlist%20mode%3Aoff seq_no%3A0 playlist_cur_index%3A0
      |playlist_timestamp%3A1517423735.80875 playlist_tracks%3A1 digital_volume_control%3A0
      |remoteMeta%3AHASH(0xc048c70) playlist%20index%3A0 id%3A-200136208
      |title%3AAll%20Along%20The%20Watchtower remote_title%3APlanet%20Rock
      |artist%3AJimi%20Hendrix%20Experience""".stripMargin.replace('\n', ' ')

  val petSounds: String =
    """09%3A14%3A2e%3Aef%3Ab3%3Aac status - 1 tags%3ANa
      |player_name%3AKitchen player_connected%3A1 player_ip%3A192.168.1.81%3A41102
      |showBriefly%3ANow%20Playing%20(1%20of%2014)%20%2CWouldn't%20It%20Be%20Nice power%3A1 signalstrength%3A0
      |mode%3Aplay time%3A6.92212028884888 rate%3A1 duration%3A153.226 can_seek%3A1 mixer%20volume%3A100
      |playlist%20repeat%3A0 playlist%20shuffle%3A0 playlist%20mode%3Aoff seq_no%3A0 playlist_cur_index%3A0
      |playlist_timestamp%3A1517424114.07508 playlist_tracks%3A14 digital_volume_control%3A0 playlist%20index%3A0
      |id%3A61344 title%3AWouldn't%20It%20Be%20Nice artist%3AThe%20Beach%20Boys""".stripMargin.replace('\n', ' ')

  val nothing: String =
    """09%3A14%3A2e%3Aef%3Ab3%3Aac status - 1 tags%3ANa
      |player_name%3AKitchen player_connected%3A1 player_ip%3A192.168.1.81%3A41105 power%3A1 signalstrength%3A0
      |mode%3Astop mixer%20volume%3A100 playlist%20repeat%3A0 playlist%20shuffle%3A0 playlist%20mode%3Aoff seq_no%3A0
      |playlist_tracks%3A0 digital_volume_control%3A0""".stripMargin.replace('\n', ' ')
}