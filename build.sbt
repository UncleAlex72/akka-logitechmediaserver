import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._
import sbt.Keys._
import sbt._

name := "akka-logitechmediaserver"

organization := "uk.co.unclealex"

scalaVersion := "2.13.0"

val catsVersion = "2.3.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % "2.6.14",
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-free" % catsVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2") ++
  Seq(
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "org.scalatest" %% "scalatest" % "3.0.8",
  ).map(_ % Test)

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  Resolver.jcenterRepo)

scalacOptions in Test ++= Seq("-Yrangepos")

fork in Test := true

// Releases

organizationHomepage := Some(url("https://bitbucket.org/UncleAlex72/"))

scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/UncleAlex72/akka-logitechmediaserver"),
    "scm:git@bitbucket.org:UncleAlex72/akka-logitechmediaserver"
  )
)
developers := List(
  Developer(
    id = "1",
    name = "Alex Jones",
    email = "alex.jones@unclealex.co.uk",
    url = url("https://bitbucket.org/UncleAlex72/")
  )
)

description := "Akka communication with logitech media servers"
licenses := List(
  "Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
homepage := Some(url("https://bitbucket.org/UncleAlex72/akka-logitechmediaserver"))

// Remove all additional repository other than Maven Central from POM
pomIncludeRepository := { _ =>
  false
}

publishTo := sonatypePublishToBundle.value

publishMavenStyle := true

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("publishLocal"),
  releaseStepCommand("publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)

